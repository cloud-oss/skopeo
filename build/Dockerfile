ARG ALPINE
ARG GOLANG

# --------------------------------
# BUILDER

FROM docker.io/library/golang:${GOLANG}-alpine${ALPINE} AS builder

# hadolint ignore=DL3018
RUN set -eux; \
    apk update --quiet; \
    apk add --no-progress --quiet --no-cache --upgrade --virtual .builddeps \
        bash \
        btrfs-progs-dev \
        gcc \
        glib-dev \
        gpgme-dev \
        make \
        musl-dev \
        ncurses \
        libselinux-dev \
        linux-headers \
        lvm2-dev \
        ostree-dev

WORKDIR /go/src/github.com/containers/skopeo

ARG SKOPEO
ARG SOURCE=https://github.com/containers/skopeo/archive/v${SKOPEO}/skopeo-${SKOPEO}.tar.gz

# https://git.alpinelinux.org/aports/tree/community/skopeo/APKBUILD?h=3.19-stable
ARG CGO_CFLAGS="$CFLAGS -D_LARGEFILE64_SOURCE"

RUN set -eux; \
    wget -q "$SOURCE"; \
    tar x -zf skopeo-${SKOPEO}.tar.gz --strip-components=1; \
    make bin/skopeo

# hadolint ignore=DL3018
RUN set -eux; \
    apk add --no-progress --quiet --no-cache --upgrade --virtual .rundeps \
        tzdata \
        ca-certificates \
        curl \
        xz

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN set -eu; \
    set +x; \
    echo "RUNTIME DEPENDENCIES"; \
    PKGNAME=$(apk info --quiet --depends .rundeps | sed -r 's/^(.*)\~.*/\1/g'); \
    for ITEM in $PKGNAME; \
    do \
        DEPENDENCY=$(apk list --installed "$ITEM" | sed -r "s/(${ITEM})-(\S+)\s.*/\1=\2/"); \
        echo "$DEPENDENCY" | tee -a /usr/local/share/rundeps; \
    done; \
    PKGNAME=$(scanelf --needed --nobanner /go/src/github.com/containers/skopeo/bin/skopeo \
        | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
        | xargs -r apk info --installed --no-network); \
    for ITEM in $PKGNAME; \
    do \
        DEPENDENCY=$(apk list --installed "$ITEM" | sed -r "s/(${ITEM})-(\S+)\s.*/\1=\2/"); \
        echo "$DEPENDENCY" | tee -a /usr/local/share/rundeps; \
    done



# --------------------------------
# FINAL IMAGE

FROM docker.io/library/alpine:${ALPINE}

ARG SKOPEO
ENV SKOPEO "${SKOPEO}"

COPY --from=builder /usr/local/share/rundeps /usr/local/share/rundeps
COPY --from=builder /go/src/github.com/containers/skopeo/bin/skopeo /usr/local/bin/

RUN set -eux; \
    xargs -a /usr/local/share/rundeps apk add --no-progress --quiet --no-cache --upgrade --virtual .rundeps

ENTRYPOINT ["/usr/local/bin/skopeo"]
CMD ["--help"]

USER 65534

LABEL org.opencontainers.image.title="skopeo" \
      org.opencontainers.image.description="Work with remote images registries" \
      org.opencontainers.image.version="$SKOPEO" \
      org.opencontainers.image.source="https://github.com/containers/skopeo" \
      org.opencontainers.image.vendor="Open Repository for Container Tools"
